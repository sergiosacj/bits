Title: Debian derivative "Lernstick" wins Swiss Open Source Education Award
Slug: lernstick-award
Date: 2015-11-10 10:00
Author: Laura Arjona Reina
Tags: Lernstick, debian, derivatives, awards
Status: draft

The Debian derivative [Lernstick](FIXME_SEE_NOTE) won the Swiss Open Source
Education Award.

Lernstick is a Debian Live derivative aimed at schools. It's primarily used in Switzerland, Austria and Germany.
Lernstick provides a mobile and secure learning and working environment that is usually installed on removable media (USB sticks, USB hard disks, SD cards, etc.) and optionally, can also be installed on internal hard drives.

The Lernstick distribution contains educational programs, multimedia software, a complete office package, games, programs for digital picture editing, Internet applications and has space for personal data.

It is developed by the University of Applied Sciences Northwestern Switzerland and educa.ch. Since its creation, it has been in continuous development. The last version has been released on 9 October 2015, and it is based on Debian 8 Jessie.

The [CH Open Source Awards](http://www.ossawards.ch/) honor companies, public entities, open source communities and individuals who have behaved bold and innovative by development or introduction of open source software. Lernstick has won the 2015 special prize Open Source Education Award, sponsored by IBM.


Links for further info:
* [Lernstick page in the Debian Derivatives Census](https://wiki.debian.org/Derivatives/Census/Lernstick)
* [Lernstick - abstract in English](http://www.imedias.ch/projekte/lernstick/lernstick_abstract_english.cfm)

NOTE
Two webs for Lernstick
* From Derivatives census: http://www.imedias.ch/projekte/lernstick/
* From CH OSS Awards : https://lernstick.educa.ch/#
(Both in German)