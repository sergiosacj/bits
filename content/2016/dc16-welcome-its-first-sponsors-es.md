Title: ¡DebConf16 da la bienvenida a sus primeros FIXME-NUMBER patrocinadores!
Slug: dc16-welcome-its-first-sponsors
Date: 2016-03-30 17:00
Author: Laura Arjona Reina
Tags: debconf16, debconf, sponsors
Lang: es
Translator: Adrià García-Alzórriz
Status: published

![DebConf16 logo](|static|/images/Dc16_703x473.png)

La DebConf16 tendrá lugar en Ciudad del Cabo, Sudáfrica, en Julio de 2016.
Tratamos de ofrecer un entorno de trabajo intenso y permitir
un buen avance para Debian y el Software libre en general. Invitamos
a quien quiera unirse y apoyar este evento.
Como conferencia llevada a cabo por voluntarios sin ánimo de lucro,
dependemos de nuestros patrocinadores.

¡Nueve organizaciones han confirmado ya su patrocinio a la DebConf16!
Presentémoslas:

Nuestro primer patrocinador platino es [**Hewlett Packard Enterprise (HPE)**](http://www.hpe.com/engage/opensource).
HPE es una de las empresas de informática más grandes del mundo,
que ofrece un amplio catálogo de productos y servicios
como servidores, almacenamiento, conectividad, consultoría
y soporte, software y servicios financieros.

HPE es también socio de desarrollo de Debian,
y ofrece hardware para el desarrollo de adaptaciones, réplicas de Debian
y otros servicios de Debian (las donaciones de hardware
se encuentran listadas en la página de las [Máquinas Debian](https://db.debian.org/machines.cgi)).

Nuestro primer patrocinador de oro es [**Valve**](http://www.valvesoftware.com/),
una empresa que desarrolla juegos, una plataforma de entretenimiento, y tecnologías para la
industria del videojuego.

Nuestro segundo patrocinador de oro es [**Google**](http://google.com), la empresa
tecnológica especializada en servicios relacionados con Internet como publicidad online y
su motor de búsqueda.

[**Rusbitech**](http://www.astralinux.com/)
(desarrolladores de la derivada de Debian Astra Linux),
[**credativ**](http://www.credativ.de/)
(una empresa orientada a los servicios haciendo énfasis en software libre y también
[socia de desarrollo de Debian](https://www.debian.org/partners/)),
[**Catalyst**](https://catalyst.net.nz/)
(una empresa que ofrece soluciones de TI usando software libre),
la [**Universidad de Ciencias aplicadas de Berna**](https://www.bfh.ch/)
(con más de [7.000](https://www.bfh.ch/en/bfh/facts_figures.html) estudiantes matriculados, situada en la capital suiza),
y [**Texas Instruments**](http://www.ti.com/)
(la empresa global de semiconductores) son nuestros cuatro patrocinadores de plata.

Y por último pero no por ello menor, la empresa de software libre [**Univention**](https://www.univention.com/)
ha confirmado su apoyo en el nivel de bronce.

## ¡Sea también un patrocinador!

¿Le gustaría ser un patrocinador? ¿Conoce o trabaja en una empresa u organización
que podría valorar ser patrocinador?

Eche un vistazo a nuestro [folleto de patrocinio](http://media.debconf.org/dc16/fundraising/debconf16_sponsorship_brochure.pdf)
(o a un [tríptico que la resume](http://media.debconf.org/dc16/fundraising/debconf16_sponsorship_flyer.pdf)),
en el que explicamos todos los detalles y se describen las ventajas para los patrocinadores.

Para más detalles, no dude en contactar con nosotros a través de [sponsors@debconf.org](mailto:sponsors@debconf.org), y
visitar la página web de la DebConf16 en [https://debconf16.debconf.org](https://debconf16.debconf.org).
