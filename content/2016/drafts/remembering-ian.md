Title: Remembering Ian Murdock
Slug: remembering-ian
Date: 2016-02-11
Author: Laura Arjona Reina, Donald Norwood
Tags: debian, ian murdock, in memoriam
Status: draft

As we are all aware Debian morned the loss of it's founder Ian Murdock. For the month of January most Debian services and outfacing visual elements kept with a darkened theme and ribbon in remembrance. Slowly into this month we are changing the websites and services back to their original themes and colors. 

Many members of the Debian Project and friends from free software communities have written about Ian and his work in the last weeks, we have gathered some of them at the end of this blog post for further reading.

# not sure if inserting here the links, or keep them at the end of the blog post

Mehdi Dogguy has mirrored Ian's personal site (http://ianmurdock.com/ ) in http://ianmurdock.debian.net so now it's hosted as part of the Debian services handled by the community.

Finally, during FOSDEM we organized a small event ["Ian Murdock, in memoriam"](https://fosdem.org/2016/schedule/event/ian_murdock/), hosted by Martin Michlmayr, who explained about the impact that Ian's vision has made in the software industry and communities, and a tribute video was screened, created by Valessio Brito with the photos and contributions of many Debian members. The event was streamed and both the slides and the recording are [available](https://fosdem.org/2016/schedule/event/ian_murdock/).

# comment: when the video is available in a free format, I'll ask the video team to store slides and video in video.debian.net and then we can change the link to there

We thank you all for grieving with us and for all these contributions, and we hope these gestures have been able to speak to the community.

Ian Murdock in Memoriam - blogposts and links

# pending to visit each link and put the titles, in the format [title](link)

https://marcin.juszkiewicz.com.pl/2015/12/30/ian-murdock-passed-away/
http://lubuntu.me/in-memory-of-ian-murdock/
http://benjaminkerensa.com/2015/12/30/remembering-ian-murdock
http://www.jonobacon.org/2015/12/30/in-memory-of-ian-murdock/
https://larjona.wordpress.com/2015/12/31/thanks-ian-thanks-debian/
http://ebb.org/bkuhn/blog/2015/12/30/ian-murdock.html
http://solydxk.com/in-memoriam-ian-murdock/
https://xanadulinux.wordpress.com/2015/12/30/ian-murdock-1973-2015/
http://blog.handylinux.org/article233/deb-sans-ian
https://perens.com/blog/2015/12/31/ian-murdock-dead/
http://codefriar.com/2016/01/01/to-ian/
http://blog.dogguy.org/2016/01/in-memoriam-ian-murdock.html
https://zgrimshell.github.io/posts/thank-you-ancenstor.html
http://www.linuxpromagazine.com/Online/Blogs/Off-the-Beat-Bruce-Byfield-s-Blog/Remembering-Ian-Murdock
http://blog.niqnutn.com/index.php?article41/ian-murdock-fondateur-du-projet-debian-1973-2015
http://amayita.livejournal.com/200544.html
http://changelog.complete.org/archives/9437-hiking-a-mountain-with-ian-murdock
https://thevarguy.com/open-source-application-software-companies/ian-murdocks-significance-free-and-open-source-software-h
http://danielpocock.com/ian-murdock-police-brutality
http://techrights.org/2015/12/30/ian-murdock/
http://www.blacklablinux.org/2015/12/in-memory-of-ian-murdock.html
http://news.siduction.org/2015/12/release-notes-for-2015-1-dev-release/
http://blog.deepin.org/2016/01/legendary-life-in-memoriam-of-debian-founder-ian-murdock/
http://www.techzim.co.zw/2016/01/in-memory-of-ian-murdock/
http://web.archive.org/web/20150318005721/http://www.loongsonclub.cn/bbs/portal.php?mod=view&aid=2




