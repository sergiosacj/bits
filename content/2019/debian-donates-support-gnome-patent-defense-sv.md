Title: Debian donerar för att ge stöd till GNOMEs patentförsvar
Slug: debian-donates-support-gnome-patent-defense
Date: 2019-10-28 17:00
Author: Sam Hartman
Tags: debian, gnome, patent trolls, fundraising, donation
Status: published
Lang:sv
Translator: Andreas Rönnquist

Idag lovar Debianprojektet att [donera] $5000 till GNOME-stiftelsen
till stöd för deras pågående patentförsvar. Den 23 oktober
[skrev vi för att uttrycka vårt stöd till GNOME] i en fråga som
påverkar hela fria mjukvarugemenskapen. Idag gör vi det stödet påtagligt.

"*Detta är större än GNOME,*" säger Debians projektledare Sam Hartman.
"*Genom att slå oss samman och visa att hela gemenskapen bakom
fri mjukvara ställer sig bakom GNOME kan vi skicka ett starkt
meddelannde till icke praktiserande enheter (patenttroll). Om du
riktar in dig på någon inom fri mjukvarugemenskapen, riktar du in
dig på alla. Vi kommer att kämpa, och kämpa för att ogilitgförklara
ditt patent. För oss gäller detta mer än pengar. Det gäller vår
frihet att bygga och distribuera vår mjukvara,*"

"*Vi är extremt tacksamma till Debian för denna generösa donation,
och även för deras stöd,*" säger Neil McGovern, verkställande direktör för
GNOME-stiftelsen. "*Det är glädjande att se att när fri mjukvara
angrips på detta sätt så samlas vi med en enad front.*"

Om GNOME behöver mer pengar senare i försvaret kommer Debian att vara
där för att ge stöd till GNOME-stiftelsen. Vi uppmuntrar individer och
organisationer att ge med och stå starka mot patenttroll.

[skrev vi för att uttrycka vårt stöd till GNOME]: https://bits.debian.org/2019/10/gnome-foundation-defense-patent-troll.html
[donera]: https://secure.givelively.org/donate/gnome-foundation-inc/gnome-patent-troll-defense-fund
