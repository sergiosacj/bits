Title: Nuevos desarrolladores y mantenedores de Debian (mayo y junio del 2019)
Slug: new-developers-2019-06
Date: 2019-08-03 10:00
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Translator: 
Status: published


Los siguientes colaboradores del proyecto se convirtieron en Debian Developers en los dos últimos meses:

  * Jean-Philippe Mengual (jpmengual)
  * Taowa Munene-Tardif (taowa)
  * Georg Faerber (georg)
  * Kyle Robbertze (paddatrapper)
  * Andy Li (andyli)
  * Michal Arbet (kevko)
  * Sruthi Chandran (srud)
  * Alban Vidal (zordhak)
  * Denis Briand (denis)
  * Jakob Haufe (sur5r)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers en los dos últimos meses:

  * Bobby de Vos
  * Jongmin Kim
  * Bastian Germann
  * Francesco Poli

¡Felicidades a todos!

