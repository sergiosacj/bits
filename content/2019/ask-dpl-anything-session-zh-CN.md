Title: 介绍新的 Debian 项目领导人（DPL），向他提问！
Date: 2019-04-26 19:20
Tags: dpl,debian-meeting
Lang: zh-CN
Translator: Boyuan Yang
Slug: ask-dpl-anything
Author: Jonathan Carter
Status: published

我们有了新的 Debian 项目领导人！2019年4月21日起，Sam Hartman 开始承担 Debian
项目领导人（DPL）的职务。

您可以在协调世界时（UTC）2019年5月10日10时加入 [OFTC](https://www.oftc.net/)
IRC 网络的 [#debian-meeting](https://webchat.oftc.net/?channels=#debian-meeting)
频道来参加对新 DPL 进行介绍的在线会议，您也能获得向他提问的机会。

您所使用的 IRC 昵称必须事先注册才能加入该频道。请参考 oftc 网站上
[注册帐号](https://www.oftc.net/Services/) 一节的内容以了解如何注册昵称。

我们计划在未来召开更多项目一级的 IRC 会议与活动。

您可以随时参考
[debian-meeting 维基页面](https://wiki.debian.org/IRC/debian-meeting)
以了解最新信息以及最新的会议日程安排。
