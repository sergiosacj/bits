Title: Debian welcomes the 2022 GSOC interns
Slug: welcome-gsoc2022-interns
Date: 2022-05-24 13:15
Author: Abhijith Pa
Tags: gsoc, announce
Image: /images/GSoC-Horizontal.png
Status: published

![GSoC logo](|static|/images/GSoC-Horizontal.png)

We are very excited to announce that Debian has selected three interns to work 
under mentorship on a variety of
[projects](https://wiki.debian.org/SummerOfCode2022/Projects) with us during the
[Google Summer of Code](https://summerofcode.withgoogle.com/).

Here are the list of the projects, interns, and details of the tasks to be performed.

----

Project: [Android SDK Tools in Debian](https://wiki.debian.org/SummerOfCode2022/ApprovedProjects/AndroidSDKToolsInDebian)

* Interns: Nkwuda Sunday Cletus and Raman Sarda

The deliverables of this project will 
mostly be finished packages submitted to Debian sid, both for new 
packages and updated packages. Whenever possible, we should also try 
to get patches submitted and merged upstream in the Android sources.

----

Project: [Project: Quality Assurance for Biological and Medical Applications inside Debian](https://wiki.debian.org/SummerOfCode2022/ApprovedProjects/QualityAssuranceDebianMed)

* Interns: Mohammed Bilal

Deliverables of the project: Continuous integration tests for all 
Debian Med applications (life sciences, medical imaging, others), 
Quality Assurance review and bug fixing.

----

Congratulations and welcome to all the interns!

The Google Summer of Code program is possible in Debian thanks to the efforts of
Debian Developers and Debian Contributors that dedicate part of their free time to
mentor interns and outreach tasks.

Join us and help extend Debian! You can follow the interns' weekly reports on the
[debian-outreach mailing-list][debian-outreach-ml], chat with us on our
[IRC channel][debian-outreach-irc] or reach out to the individual projects' team
mailing lists.

[debian-outreach-ml]: http://lists.debian.org/debian-outreach/ (debian-outreach AT lists.debian.org)
[debian-outreach-irc]: irc://irc.debian.org/debian-outreach (#debian-outreach on irc.debian.org)
