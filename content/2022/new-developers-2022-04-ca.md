Title: Nous desenvolupadors i mantenidors de Debian (març i abril del 2022)
Slug: new-developers-2022-04
Date: 2022-05-13 17:00
Author: Jean-Pierre Giraud
Tags: project
Lang: ca
Translator: 
Status: published


Els següents col·laboradors del projecte han esdevingut Debian Developers en els darrers dos mesos:

  * Henry-Nicolas Tourneur (hntourne)
  * Nick Black (dank)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers en els darrers dos mesos:

  * Jan Mojžíš
  * Philip Wyett
  * Thomas Ward
  * Fabio Fantoni
  * Mohammed Bilal
  * Guilherme de Paula Xavier Segundo

Enhorabona a tots!

