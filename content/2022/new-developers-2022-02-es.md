Title: Nuevos desarrolladores y mantenedores de Debian (enero y febrero del 2022)
Slug: new-developers-2022-02
Date: 2022-03-21 17:00
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Translator: 
Status: published


El siguiente colaborador del proyecto se convirtió en Debian Developer en los dos últimos meses:

  * Francisco Vilmar Cardoso Ruviaro (vilmar)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers en los dos últimos meses:

  * Lu YaNing
  * Mathias Gibbens
  * Markus Blatt
  * Peter Blackman
  * David da Silva Polverari

¡Felicidades a todos!

