Title: Những nhà Phát triển và Bảo trì Debian mới (Tháng bảy và tám 2021)
Slug: new-developers-2021-08
Date: 2021-09-13 17:00
Author: Jean-Pierre Giraud
Tags: project
Lang: vi
Translator: Trần Ngọc Quân
Status: published


Trong hai tháng qua, những người đóng góp sau đây nhận được tài khoản Phát triển Debian:

  * Aloïs Micard (creekorful)
  * Sophie Brun (sophieb)

Trong hai tháng qua, những người đóng góp sau đây được thêm vào với tư cách là người Bảo trì Debian:

  * Douglas Andrew Torrance
  * Marcel Fourné
  * Marcos Talau
  * Sebastian Geiger

Xin chúc mừng!

