Title: O Debian 11 "bullseye" foi lançado!
Date: 2021-08-14 23:30
Tags: bullseye
Slug: bullseye-released
Author: Ana Guerrero Lopez, Laura Arjona Reina and Jean-Pierre Giraud
Translator: Paulo Henrique de Lima Santana (phls)
Artist: Juliette Taka
Status: published
Lang: pt-BR

[![Alt Bullseye has been released](|static|/images/banner_bullseye.png)](https://deb.li/bullseye)

Estamos felizes em anunciar o lançamento do Debian 11, codinome *bullseye*!

**Quer instalar? **
Escolha sua [mídia de instalação](https://www.debian.org/distrib) favorita
e leia o [manual de instalação](https://www.debian.org/releases/bullseye/installmanual).
Você também pode usar uma imagem oficial da nuvem diretamente no seu provedor de
nuvem ou experimentar o Debian antes de instalá-lo usando nossas imagens "live".

**Já é um(a) usuário(a) Debian feliz e deseja apenas atualizar? **
Você pode facilmente atualizar sua instalação atual do Debian 10 "buster";
por favor leia as [notas de lançamento](https://www.debian.org/releases/bullseye/releasenotes).

**Você quer comemorar o lançamento?**
Fornecemos alguns [materiais gráficos do bullseye](https://wiki.debian.org/DebianArt/Themes/Homeworld)
que você pode compartilhar ou usar como base para suas próprias criações.
Acompanhe a conversa sobre o bullseye nas redes sociais através das hashtags
\#ReleasingDebianBullseye e #Debian11Bullseye
ou junte-se a uma [festa de lançamento](https://wiki.debian.org/ReleasePartyBullseye) pessoalmente ou online.