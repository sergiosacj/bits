Title: Nuevos mantenedores de Debian (noviembre y diciembre del 2020)
Slug: new-developers-2020-12
Date: 2021-01-22 18:00
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Translator: 
Status: published


Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers en los dos últimos meses:

  * Timo Röhling
  * Fabio Augusto De Muzio Tobich
  * Arun Kumar Pariyar
  * Francis Murtagh
  * William Desportes
  * Robin Gustafsson
  * Nicholas Guriev

¡Felicidades a todos!

