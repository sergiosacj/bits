Title: Nuevos desarrolladores y mantenedores de Debian (septiembre y octubre del 2020)
Slug: new-developers-2020-10
Date: 2020-11-16 20:00
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Translator: 
Status: published


Los siguientes colaboradores del proyecto se convirtieron en Debian Developers en los dos últimos meses:

  * Benda XU (orv)
  * Joseph Nahmias (jello)
  * Marcos Fouces (marcos)
  * Hayashi Kentaro (kenhys)
  * James Valleroy (jvalleroy)
  * Helge Deller (deller)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers en los dos últimos meses:

  * Ricardo Ribalda Delgado
  * Pierre Gruet
  * Henry-Nicolas Tourneur
  * Aloïs Micard
  * Jérôme Lebleu
  * Nis Martensen
  * Stephan Lachnit
  * Felix Salfelder
  * Aleksey Kravchenko
  * Étienne Mollier

¡Felicidades a todos!

