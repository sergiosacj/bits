Title: O salsa CI agora inclui suporte de construção para i386
Slug: salsa-ci-i386-build
Date: 2020-10-09 20:20
Author: Salsa CI Team
Translator: Carlos Henrique Lima Melara
Lang: pt-BR
Tags: salsa, i386
Status: published

![Salsa CI pipeline com suporte de construção para i386](|static|/images/salsa-ci-i386-build.png)

O salsa CI almeja melhorar o ciclo de empacotamento no Debian ao oferecer
testes de integração contínua totalmente compatíveis com o empacotamento Debian.
O projeto principal do salsa CI é o
[pipeline](https://salsa.debian.org/salsa-ci-team/pipeline/), que constrói
pacotes e realiza diferentes testes após cada `git push` para o salsa.
O pipeline permite ter respostas rápidas e antecipadas sobre quaisquer
problemas que as novas mudanças possam ter criado ou resolvido, sem a
necessidade de enviar para o repositório.

Todas as tarefas do pipeline rodam na arquitetura `amd64`, mas o time salsa CI
adicionou recentemente suporte para a construção de pacotes também na
arquitetura `i386`.
Este trabalho começou durante o sprint do time salsa CI na DebConf20 após a
[palestra "Where is Salsa CI right now"](https://debconf20.debconf.org/talks/47-where-is-salsa-ci-right-now/),
sendo necessárias diferentes alterações no núcleo do pipeline para tornar isso
possível.
Para mais detalhes, veja o merge request relacionado:
[https://salsa.debian.org/salsa-ci-team/pipeline/-/merge_requests/256](https://salsa.debian.org/salsa-ci-team/pipeline/-/merge_requests/256)

Se você tiver qualquer dúvida, pode contatar o time salsa CI no canal #salsaci
em irc.oftc.net
