Title: Canais de comunicação oficiais o Debian
Slug: official-communication-channels
Date: 2020-03-16 14:00
Author: Laura Arjona Reina, Ana Guerrero Lopez e Donald Norwood
Tags: project, announce
Translator: Thiago Pezzo (tico), Paulo Henrique de Lima Santana (phls)
Lang: pt-BR
Status: published

De tempos em tempos, recebemos perguntas no Debian sobre nossos canais oficiais
de comunicação e perguntas sobre o status do Debian relativo a quem pode
ter sites com nomes semelhantes.

O site principal do Debian [www.debian.org](https://www.debian.org)
é o nosso principal meio de comunicação. Quem procura informações sobre os
atuais eventos e sobre o progresso do desenvolvimento na comunidade pode se
interessar na seção [Notícias do Debian](https://www.debian.org/News/)
(Debian news) no site do Debian.
Para anúncios menos formais, temos o blog oficial do Debian
[Bits do Debian](https://bits.debian.org), e o serviço
[Micronotícias do Debian](https://micronews.debian.org) (Debian Micronews)
para notas mais curtas.

Nosso boletim oficial
[Notícias do Projeto Debian](https://www.debian.org/News/weekly/) (Debian
Project News) e todos os anúncios oficiais de notícias ou mudanças no projeto
são duplamente postados em nosso site e em nossas listas de e-mail oficiais
[debian-announce](https://lists.debian.org/debian-announce/) ou
[debian-news](https://lists.debian.org/debian-news/).
A postagem nessas listas de discussão é restrita.

Também queremos aproveitar a oportunidade para anunciar como o Projeto Debian,
ou para resumir, Debian, é estruturado.

O Debian tem uma estrutura regulada pela nossa
[Constituição](https://www.debian.org/devel/constitution).
Diretores(as) e membros(as) oficiais e delegados(as) estão listados(as) em
nossa [Estrutura Organizacional](https://www.debian.org/intro/organization).
Equipes adicionais estão listadas em nossa página
[Equipes](https://wiki.debian.org/Teams).

A lista completa de membros(as) oficiais do Debian pode ser encontrada em nossa
[Página de novos(as) membros(as)](https://nm.debian.org/members),
onde nossa associação é gerenciada. Uma lista mais ampla de contribuidores(as)
Debian pode ser encontrada em nossa página
[Contribuidores(as)](https://contributors.debian.org).

Em caso de dúvidas, convidamos você a entrar em contato com a equipe de imprensa
em [press@debian.org](mailto:press@debian.org).
