Title: Nya Debianutvecklare och Debian Maintainers (Maj och Juni 2020)
Slug: new-developers-2020-06
Date: 2020-07-21 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: sv
Translator: Andreas Rönnquist
Status: published


Följande bidragslämnare fick sina Debianutvecklarkonton under de senaste två månaderna:

  * Richard Laager (rlaager)
  * Thiago Andrade Marques (andrade)
  * Vincent Prat (vivi)
  * Michael Robin Crusoe (crusoe)
  * Jordan Justen (jljusten)
  * Anuradha Weeraman (anuradha)
  * Bernelle Verster (indiebio)
  * Gabriel F. T. Gomes (gabriel)
  * Kurt Kremitzki (kkremitzki)
  * Nicolas Mora (babelouest)
  * Birger Schacht (birger)
  * Sudip Mukherjee (sudip)

Följande bidragslämnare adderades som Debian Maintainers under de senaste två månaderna:

  * Marco Trevisan
  * Dennis Braun
  * Stephane Neveu
  * Seunghun Han
  * Alexander Johan Georg Kjäll
  * Friedrich Beckmann
  * Diego M. Rodriguez
  * Nilesh Patra
  * Hiroshi Yokota

Grattis!

