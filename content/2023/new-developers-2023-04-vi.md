Title: Những nhà Phát triển và Bảo trì Debian mới (Tháng ba và tư 2023)
Slug: new-developers-2023-04
Date: 2023-05-25 12:00
Author: Jean-Pierre Giraud
Tags: project
Lang: vi
Translator: Trần Ngọc Quân
Status: published


Trong hai tháng qua, những người đóng góp sau đây nhận được tài khoản Phát triển Debian:

  * James Lu (jlu)
  * Hugh McMaster (hmc)
  * Agathe Porte (gagath)

Trong hai tháng qua, những người đóng góp sau đây được thêm vào với tư cách là người Bảo trì Debian:

  * Soren Stoutner
  * Matthijs Kooijman
  * Vinay Keshava
  * Jarrah Gosbell
  * Carlos Henrique Lima Melara
  * Cordell Bloor

Xin chúc mừng!

