Title: Nouveaux développeurs et mainteneurs de Debian (novembre et décembre 2022)
Slug: new-developers-2022-12
Date: 2023-01-24 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Translator: 
Status: published


Les contributeurs suivants sont devenus développeurs Debian ces deux derniers mois :

  * Dennis Braun (snd)
  * Raúl Benencia (rul)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la même période :

  * Gioele Barabucci
  * Agathe Porte
  * Braulio Henrique Marques Souto
  * Matthias Geiger
  * Alper Nebi Yasak
  * Fabian Grünbichler
  * Lance Lin

Félicitations !

