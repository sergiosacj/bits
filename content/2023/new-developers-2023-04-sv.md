Title: Nya Debianutvecklare och Debian Maintainers (Mars och April 2023)
Slug: new-developers-2023-04
Date: 2023-05-25 12:00
Author: Jean-Pierre Giraud
Tags: project
Lang: sv
Translator: Andreas Rönnquist
Status: published


Följande bidragslämnare fick sina Debianutvecklarkonton under de senaste två månaderna:

  * James Lu (jlu)
  * Hugh McMaster (hmc)
  * Agathe Porte (gagath)

Följande bidragslämnare adderades som Debian Maintainers under de senaste två månaderna:

  * Soren Stoutner
  * Matthijs Kooijman
  * Vinay Keshava
  * Jarrah Gosbell
  * Carlos Henrique Lima Melara
  * Cordell Bloor

Grattis!

