15:56 < ana> highvoltage: hi, I have ended up adding a subsite in bits instead of using the categories.
             This is a good idea because it'll make the DPL subsite more independant, you can fully
             change the sidebar and the theme
15:57 < ana> you'd need to clone the bits repository, all the files are under dpl/, you can build only
             this subsite to do tests. Add your posts in content/2020, images in content/images and to
             build the subblog use the makefile: make html && make serve
15:58 < ana> to build the full blog, you'll have to go to the root directory of bits and use make
             'html-full'
15:58 < ana> it'll build bits, bits/dpl and serve it locally in localhost:8000
15:59 < ana> this will show the normal bits website, with the dpl subsite in localhost:8000/dpl
15:59 < ana> all this is in the branch "dpl"

